# We need to manipulate OrbitaStruct
import orbita_struct
import atexit
# import orbita_generators
from orbita_sender import SenderController
orbita = orbita_struct.OrbitaStruct.get_instance()
atexit.register(orbita_struct.save_state,orbita)

def status_request(*args):
    code=b'\x01'
    version=b'\x01'
    modules_count=b'\x04'
    mask=b'\x00\x0F'
    response = b''.join([code,version,modules_count,mask])
    return response
status_request.id=b'\x01'

def module_status_request(*args):
    num = args[0]
    code = b'\x01' # пока без ошибок
    mod = orbita.module_by_num(num)
    if mod:
        return b''.join([code, mod.num, mod.INF_A.value, mod.INF_B.value,
                        mod.VIDEO.value, mod.liter.value, mod.inf.value,
                        mod.FORCED_A.value, mod.FORCED_B.value])
    return b''.join([code,num])
module_status_request.id=b'\x02'

def set_module_state(args):
    num = args[0:1]
    code = b'\x01' # пока без ошибок
    mod = orbita.module_by_num(num)
    state = orbita_struct.ChannelState
    mod.INF_A = state(args[1:2])
    mod.INF_B = state(args[2:3])
    mod.VIDEO = state(args[3:4])
    mod.liter = orbita_struct.Liter(args[4:5])
    mod.inf = orbita_struct.Informativeness(args[5:6])
    mod.FORCED_A = state(args[6:7])
    mod.FORCED_B = state(args[7:8])
    SenderController.modules_changing_event.set()
    return b"".join([code, num])
set_module_state.id=b'\x03'

def akzti_turn_on(args):
    print("AKZTI turn on")
    print(args)
    code = b'\x01'
    orbita.akzti.status=args[0:1]
    orbita.akzti.mode=orbita_struct.AKZTImode(args[2:3])
    orbita.akzti.pdb_inf=args[1:2]
    return code
akzti_turn_on.id=b'\x10'

def akzti_status_request(*args):
    code = b'\x03'
    status = orbita.akzti.status.value
    source = orbita.akzti.source.value
    pdb_inf = orbita.akzti.pdb_inf.value
    mode = orbita.akzti.mode.value

    result = b''.join([code,status,pdb_inf,mode])
    return result
akzti_status_request.id=b'\x11'

def start_record(*args):
    action = args[0]
    if action == b'\x01': # команда включения записи
        print("STARTING RECORD")
        #channels = orbita.active_channels()
        #for channel in channels:
        #    sender = SenderThread(channel[0]) #port
        #    sender.start()
    else:
        print("Stopping record")
    return b'\x01'
start_record.id=b'\x09'


