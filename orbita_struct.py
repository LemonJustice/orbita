# Здесь описана структура НТС, в частности состояние приемных модулей

from enum import Enum
import pickle

class OrbitaStruct:
    modules_number = [b'\x01', b'\x02', b'\x03', b'\x04']
    save_filename = "test"
    __instance = None
    def __init__(self):
        self.modules = list(ReceivingModule(n,Liter.Lit44.value, Informativeness.M08.value)\
                            for n in OrbitaStruct.modules_number)
        self.akzti = Akzti()
        self.ports_available = list(range(40001,40013))
        self.modules[0].INF_A = ChannelState.ON
    #pass

    @staticmethod
    def get_instance():
        if OrbitaStruct.__instance:
            return OrbitaStruct.__instance
        try:
            with open(OrbitaStruct.save_filename, 'rb') as save:
                instance = pickle.load(save)
            print("Loaded state from file.")
        except:
            print("Could not load state from file, creating default state")
            instance = OrbitaStruct()
        OrbitaStruct.__instance = instance
        return instance

    def module_by_num(self, num):
        return next((m for m in self.modules if m.num == num), None)

    def all_active_channels(self):
        channels = list((mod.inf, channel) for mod in self.modules\
                        for channel in mod.channels() )
        # формат channels: (информативность, (on/off, 'имя_канала'))
        channels_ports = zip(self.ports_available, channels)
        # формат active: {порт: (инф, 'имя_канала')}
        active = dict((port, channel) for port, channel in channels_ports if channel[1][0] == ChannelState.ON)
        return active

def save_state(instance):
    with open(OrbitaStruct.save_filename, 'wb') as save:
        pickle.dump(instance, save)
    print("Saved state of structure to file.")

class ReceivingModule:
    def __init__(self, num, liter, inf):
        self.num = num
        self.liter = liter
        self.inf = inf
        self.INF_A = ChannelState.OFF
        self.INF_B = ChannelState.OFF
        self.VIDEO = ChannelState.OFF
        self.FORCED_A = ChannelState.OFF
        self.FORCED_B = ChannelState.OFF


    @property
    def num(self):
        return self.__num

    @num.setter
    def num(self, n):
        self.__num = n

    @property
    def inf(self):
        return self.__inf

    @inf.setter
    def inf(self, inf):
        self.__inf = Informativeness(inf)

    @property
    def liter(self):
        return self.__liter

    @liter.setter
    def liter(self,lit):
        self.__liter = Liter(lit)

    @property
    def INF_A(self):
        return self.__INF_A

    @INF_A.setter
    def INF_A(self,inf_a):
        self.__INF_A = ChannelState(inf_a)

    @property
    def INF_B(self):
        return self.__INF_B

    @INF_B.setter
    def INF_B(self,inf_b):
        self.__INF_B = ChannelState(inf_b)

    @property
    def VIDEO(self):
        return self.__video

    @VIDEO.setter
    def VIDEO(self,video):
        self.__video = ChannelState(video)

    @property
    def FORCED_A(self):
        return self.__forced_a

    @FORCED_A.setter
    def FORCED_A(self,forced_a):
        self.__forced_a = ChannelState(forced_a)

    @property
    def FORCED_B(self):
        return self.__forced_b

    @FORCED_B.setter
    def FORCED_B(self,forced_b):
        self.__forced_b = ChannelState(forced_b)

    def channels(self):
        return [(self.INF_A, 'INF_A'), (self.INF_B, 'INF_B'), (self.VIDEO, 'VIDEO')]

class Akzti():
    def __init__(self):
        self.status = ChannelState.OFF
        self.pdb_inf = Informativeness.M08
        self.mode = AKZTImode.NO_PARAMS
        self.source = Source.NTS

    @property
    def status(self):
        return self.__status

    @status.setter
    def status(self,status):
        self.__status = ChannelState(status)

    @property
    def pdb_inf(self):
        return self.__pdb_inf

    @pdb_inf.setter
    def pdb_inf(self,pdb_inf):
        self.__pdb_inf = Informativeness(pdb_inf)

    @property
    def mode(self):
        return self.__mode

    @mode.setter
    def mode(self, mode):
        self.__mode = AKZTImode(mode)

    @property
    def source(self):
        return self.__source

    @source.setter
    def source(self, source):
        self.__source = Source(source)


class Informativeness(Enum):
    M04 = b'\x04'
    M08 = b'\x08'
    M16 = b'\x16'

class Liter(Enum):
    Lit44 = b'\x44'
    Lit54 = b'\x54'
    Lit64 = b'\x64'
    Lit74 = b'\x70'

class ChannelState(Enum):
    ON = b'\x01'
    OFF = b'\x00'

class AKZTImode(Enum):
    PARAMS = b'\x01'
    NO_PARAMS = b'\x02'

class Source(Enum):
    NTS = b'\x01'
    PDB = b'\x02'
