# The main server

import orbita_parser
import socket
import threading
import sys
import select
from orbita_sender import SenderController
from time import sleep, clock

host='localhost'
port=100
# пока поместим shutdown event только сюда, треды передачи данных могут прерываться сами.
shutdown_event = threading.Event()

class OrbitaRequestThread(threading.Thread):
    def __init__(self,request):
        self.request_parser = orbita_parser.CommandRequestParser()
        self.request = request
        super().__init__()

    def run(self):
        print("started new request thread")
        request = self.request
        while not shutdown_event.is_set():
            if shutdown_event.is_set():
                print("We got the signal at least")
            data = request.recv(50)
            if not data: # это происходит при завершении программы:
                # данные из сокета еще можно получить, но там пусто
                print("Finished working with request")
                break
            result = self.handle(data)
            if result:
                request.sendall(result)
        print("Closing request")
        #request.close()

    def handle(self,data):
        if not data:
            return
        data_arr=bytearray(data)
        counter = bytes([data_arr.pop(0)])
        # first two bytes are counter and id
        comm_id = bytes([data_arr.pop(0)])
        resp = self.request_parser.parse_request(comm_id,data_arr)
        full_resp = b"".join([counter,comm_id,resp])
        return full_resp

class OrbitaCommandServer(threading.Thread):
    def run(self):
        self.serve()

    def serve(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        #self.socket.setsockopt(socket.SO_REUSEADDR, socket.SO_REUSEPORT)
        self.socket.bind((host,port))
        self.socket.listen(0)
        self.conn = None
        print("Activating command server")
        print(self.socket)
        sender_controller = SenderController()
        sender_controller.start()
        try:
            while True:
                (self.conn, self.addr) = self.socket.accept()
                print("Connected by ",self.conn)
                rt = OrbitaRequestThread(self.conn)
                rt.start()
        except KeyboardInterrupt:
            start = clock()
            print("Closing connection")
            shutdown_event.set()
            if self.conn:
                rt.join() # Ждем, пока реквест-тред завершится нормально
                self.conn.close() #
            sender_controller.shutdown.set()
            sys.exit(0)

def main():
    orbita_server = OrbitaCommandServer()
    print("threads: ", threading.enumerate())
    orbita_server.start()

if __name__ == '__main__':
    #main_thread = threading.Thread(target=main())
    #main_thread.start()
    orbita_server = OrbitaCommandServer()
    orbita_server.run()
