import inspect
import orbita_funcs

class CommandRequestParser:
    all_functions = list(inspect.getmembers(orbita_funcs,\
            predicate = lambda x: inspect.isfunction(x) and 'id' in x.__dict__))
    # берем только те функции из модуля orbita_funcs, к которым привязан id команды
    commands = dict((func[1].id, func[1]) for func in all_functions)
    # func[1] т.к. inspect возвращает tuple из имени и функции

    def parse_request(self,command_id, args):
        ids = list(CommandRequestParser.commands.keys())
        if command_id not in ids:
            return b'\xFF'
        # находим функцию, обрабатывающую команду command_id
        func=CommandRequestParser.commands[command_id]
        # вызываем ее
        response = func(args)
        return response

