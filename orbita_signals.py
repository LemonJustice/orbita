# Сигналы и все что с ними связано
from orbita_bitstring import Bitstring as bitstring
from collections import namedtuple
import random
import helpers
from functools import partial

def endless(func):
    def endless_calls(*args, **kwargs):
        while True:
            val = func(*args, **kwargs)
            func_values = iter(val)
            yield from func_values
    return endless_calls

class Signal:
    def __init__(self, bits_used, value_func, *value_func_args, fit_word, bitwidth=0):
        """
        bits_used - список позиций битов в слове, занятых этим сигналом
        fit_word - свойство сигнала, определяющее, должно ли одно значение сигнала помещаться в слово.
        Аналоговые сигналы ведут себя так, цифровые сигналы и ВТ размещают одно значение в нескольких словах
        Warning: никаких гарантий того, что возвращаемые value_func значения поместятся в указаное кол-во бит, нет.
        Если такое произойдет с сигналом, скорее всего, он будет просто странно выглядеть на графике.
        value_func - функция (c аргументами или partial), отвечающая за вычисление значений сигнала
        Определение функции должно быть обязательно обернуто в endless; функция должна возвращать список
        value_func_args - аргументы для value_func
        """
        self.bits = bits_used
        self.value_func = value_func
        self.value_args = value_func_args
        self.bitwidth = bitwidth
        if fit_word:
            self.bitwidth = len(self.bits)
        bit_generator = self.fetch_bit()
        self.bit_generator = bit_generator

    def fetch_bit(self):
        # note: генераторы вместо обычных функций здесь затем, чтобы не получать сразу полное значение функции и не хранить его.
        # оно может быть длинным.
        arglist = self.value_args
        print("Args passed to value func: ", arglist)
        func = endless(self.value_func)
        for val in func(*arglist):
            value_bits = bitstring(val, self.bitwidth)
            for bit in value_bits:
                yield bit


class DigitaSignal(Signal):
    """   1 - много адресов, вместо адреса получается address space
    2 - много value_func Для цифровых сигналов value - это пакет байт. Много пакетов - много функций для их генерации
    и они выдают их на рандоме
    и у них есть pause_code
    """
    def __init__(self,pause_code, addresses, packets, bit_9 = False):
        """
        Здесь packets могут быть как байтами, так и функциями,
        возвращающими байты (или списки int)
        если у этих функций есть аргументы, лучше самостоятельно сделать из них партиал
        Пока так, в случае крайней необходимости можно запилить
        обработку аргументов для множества функций
        pause_code - это int в двоичной форме, вроде 0b01101010111
        """
        self.address_space = self.create_address_space(addresses, 131072)
        self.pause_code = pause_code
        value_func = self.digital_superfunc
        value_func_args = packets
        self.bit_9 = bit_9
        bits_used = [3, 4, 5, 6, 7, 10, 11, 12, 13, 14, 15]
        if self.bit_9:
            bits_used = [2, 3, 4, 5, 6, 7, 10, 11, 12, 13, 14, 15]
        super().__init__(bits_used, value_func, *value_func_args, fit_word = True)

    def create_address_space(self,addresses, cadre_length):
        # это что-то вроде merge_sort
        # здесь последовательности - это заданые адреса
        # формируем из них единое адресное пространство
        element = namedtuple("element", ["el_index", "value"])
        # словарь последовательностей с индексами - чтобы можно было по индексу доставать последовательность
        sequences = {index : iter(range(addr[0], cadre_length, addr[1])) \
                                for index, addr in enumerate(addresses)}
        # кэш - список элементов с индексами, чтобы удобнее было сортировать
        cache = [element(index, next(sequence)) for index, sequence in sequences.items()]
        result = []
        needs_sort = True

        while any(sequences.values()): # внезапно, any = False для единственного элемента с ключом 0
            if needs_sort:
                cache.sort(key=lambda x: x.value)
            min_element = cache.pop(0)
            result.append(min_element.value)
            index = min_element.el_index
            try:
                new_element = element(index, next(sequences[index]))
                if len(sequences)==0 | new_element.value > cache[-1].value:
                    needs_sort = False
                else:
                    needs_sort = True
                cache.append(new_element)
            except StopIteration: # одна из последовательностей кончилась, прекращаем с ней работать
                del sequences[index]
        return result

    def digital_superfunc(self, *packets):
        # Эта функция просто выдает на рандоме один из переданых пакетов или код паузы
        # Каждый байт пакета должен быть обернут служебными битами справа и слева
        # его bitstring должен быть длиной 12 в итоге, и занимать слово
        # TODO: это можно доработать в виде генератора, чтобы не делать лишнего каждый раз при вызове
        def new_val(val):
            #no 9 bit: add 0 to end 
            if not self.bit_9:
                val = (val << 1)
            #and buffer overflow (0 so far)
            new_val = (val << 1)
            parity_val = 0
            for i in range(0,new_val.bit_length()):
                parity_val+=(new_val >> i) & 1
            parity = not (parity_val % 2)
            new_val = (new_val << 1) | parity
            return new_val

        def transform(b: bytes):
            new_bytes = [ new_val(val) for val in b ] 
            # на самом деле это уже list(int), т.к. в байт оно не помещается
            # но это норм
            return new_bytes
            
        # в качестве значения цифровых пакетов можно передавать функции
        new_packets_list = [new_val(p()) if helpers.is_callable(p)\
                else transform(p) for p in packets] 
        # Сделаем код паузы чуть более вероятным
        packet_probabilities = [(3, packet) for packet in new_packets_list]
        packet_probabilities.append((0, [self.pause_code]))

        weighted_choice_list = [val for cnt, val in packet_probabilities for i in range(cnt)]
        result = random.choice(weighted_choice_list)
        #result = [self.pause_code]
        #print("Result ", result)
        return result # без обертки в список, т.к. цифровой пакет - это уже bytes, iter() будет возвращать int


class AnalogSignal(Signal):
    """ Обычные аналоговые сигналы с одним адресом (fw, step)
        fw - первое слово сигнала
        step - шаг между словами
    """
    def __init__(self, fw, step, bits_used, value_func, *value_func_args, cadre_length=131072): # cadre_length - заглушка
        """
        Некоторым периодическим функциям требуется информация
         о том, сколько слов в кадре будет занимать сигнал
         для расчета нужной частоты (например, пила, меандр)
         Для того, чтобы явно не передавать / не высчитывать это, делаем это автоматически
         Такие функции декорируются @needs_frame_info с именем параметра
         в который нужно положить данное значение
        """
        if hasattr(value_func,"needs_frame_info"):
             paramname=value_func.needs_frame_info
             signal_frame_length = cadre_length // step
             real_value_func=partial(value_func,**{paramname: signal_frame_length})
        else:
            real_value_func = value_func
        super().__init__(bits_used, real_value_func, *value_func_args, fit_word = True)
        self.fw = fw
        self.step = step

#_____________________
# Набор функций для аналоговых сигналов


class VTSignal(Signal):
    """
        Особенности сигналов ВТ:
        -у них свой способ задания адресов (через номер фразы)
        - для некоторых нужны особенные функции
    """
    def __init__(self, phrases, value_func, *value_func_args):
        bits_used = [9]
        self.phrase_numbers = phrases
        super().__init__(bits_used, value_func, *value_func_args, fit_word = False, bitwidth = len(self.phrase_numbers))

