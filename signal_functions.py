# Really we should not compute a lot of this stuff every time. After setting things up, everything is known.
def needs_frame_info(param):
    def decorated(func):
        func.needs_frame_info = param
        return func
    return decorated

def constant(value):
    return [value]

@needs_frame_info("frame_size")
def meandr(min, max, target_freq, frame_size):
    base_freq = frame_size // 2
    if target_freq > base_freq:
        print("Cant carry %s hz signal on %s hz", target_freq, base_freq)
        return []
    meandr_width = base_freq // target_freq
    result = [min]*meandr_width+[max]*meandr_width
    return result

@needs_frame_info("frame_size")
def saw(min, max, target_freq, frame_size):
    base_freq = frame_size // 2
    if target_freq > base_freq:
        print("Cant carry %s hz signal on %s hz", target_freq, base_freq)
        return [0]
    saw_width = base_freq // target_freq
    saw_vector = range(min, max) # изначальное множество значений для 1 периода пилы
    amplitude = len(saw_vector)# амплитуда

    multiplier = saw_width / amplitude # сколько точек приходится на один уровень
    if multiplier>1: # пример - нужно разместить 64 значения (0..63) в 128 точках
        result = list(val for val in saw_vector for n in range(0,int(multiplier)))
        return result
    else: # доступных точек меньше, чем значений
        num_of_points = int(amplitude * multiplier) # пример - нужно разместить 64 значения в 16 точках
        val_per_point, remainder = divmod(amplitude, num_of_points)
        result = list(saw_vector[::val_per_point])
        if remainder!=0:
            result.append(result[-1]+remainder)
            return result
        else:
            return result

def aru_function(counter, min_value, max_value):
    print("called first time")
    for val in range(min_value, max_value):
        for i in range(counter):
            yield val

def digital_cycle(min, max):
    # just returns elements from argument iter endlessly
    vals = [min, max]

