class Bitstring:
    def __init__(self, b, bitlength):
        self.val = b
        self.length = bitlength

# TODO Ограничения на length и value
    @property
    def bin(self):
        bin_string = '{:0{l}b}'.format(self.val, l=self.length)
        return bin_string

    def set_bit(self, pos, bit):
        # bit is True or False
        pos=self.length-pos-1
        self.toggle_bit(pos) if bit else self.clear_bit(pos)

    def toggle_bit(self, pos):
        self.val |= 1 << pos

    def clear_bit(self, pos):
        self.val &= ~(1 << pos)

    def bytes(self):
        return bytes([self.val])

    def __iter__(self):
        for el in self.bin:
            bin_val = bool(int(el))
            yield bin_val

    def __repr__(self):
        return self.bin

    def __getitem__(self, key):
        return self.bin[key]

    def __setitem__(self, key, val):
        if not isinstance(val, bool):
            raise ValueError("Only boolean values are accepted")
        self.set_bit(key, val)
