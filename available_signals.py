from signal_functions import *
from signal_types import *
from orbita_signals import AnalogSignal, DigitaSignal, VTSignal


analog = [ AnalogSignal(0, 512, ANP.T01.value, meandr, 60, 445, 1),\
          AnalogSignal(16, 128, ANP.T01.value, saw, 0, 64, 0.5),\
          #AnalogSignal(16, 128, ANP.T01.value, constant, 255),\
          AnalogSignal(32, 128, ANP.T01.value, constant, 136),\
          AnalogSignal(64, 128, ANP.T01.value, constant, 183),\
          #AnalogSignal(9, 512, ANP.T01.value, constant, 120),\
          #AnalogSignal(128, 128, ANP.T01.value, constant, 191),\
          #AnalogSignal(3, 16, BMP22.T01.value, saw, 0, 64, 4),\
          #AnalogSignal(3, 16, BMP22.T02.value, saw, 0, 64, 16)
         ]
digital = [ DigitaSignal(0b01010101011, [(0, 128)], [b'\xae\x2c\xdd'])] # address is A10B12C10D21E13X13
