import threading
import socketserver
import orbita_struct
from time import sleep
from orbita_generators import generator_in_bytes

orbita = orbita_struct.OrbitaStruct.get_instance()

class SenderController(threading.Thread):
    modules_changing_event = threading.Event()
    modules_changing_event.clear()

    def __init__(self):
        self.active = orbita.all_active_channels()
        self.threads=[]
        super().__init__()
        self.shutdown = threading.Event()
        self.shutdown.clear()
        print("Created Server Controller")

    def launch_all(self):
        print("THREADS BEFORE START: ", threading.enumerate())
        if any(self.active):
            print(self.active)
            for port, channel in self.active.items():
                self.launch_thread(port, channel)
        else:
            print("Nothing to start")
        print("THREADS AFTER START: ", threading.enumerate())

    def launch_thread(self, port, channel):
        print("Launcing thread with port ", port)
        sender = SenderThread(port = port, channel_name = channel[0],\
                              inf = channel[1])
        self.threads.append(sender)
        sender.start()

    def restart_all(self):
        print("Stopping em all")
        self.stop_all()
        print("Starting em all")
        self.launch_all()

    def stop_all(self):
        if any(self.threads):
            print("THREADS BEFORE SHUTDOWN: ", threading.enumerate())
            for t in self.threads:
                print("About to shut down ", t)
                t.server.shutdown()
                t.server.server_close()
        print("THREADS AFTER SHUTDOWN: ", threading.enumerate())
        self.threads = []

    def run(self):
        print("Starting Sender Controller.")
        self.launch_all()

        while not self.shutdown.is_set():
            if SenderController.modules_changing_event.is_set():
                SenderController.modules_changing_event.clear() # reset event flag
                if self.active != orbita.all_active_channels():
                    print("Restart Everything")
                    self.active = orbita.all_active_channels()
                    self.restart_all()
        # stop the senders
        self.stop_all()

class SenderThread(threading.Thread):
    def __init__(self,**kwargs):
        self.port = kwargs['port']
        self.server = SenderServer(('localhost', self.port), SenderHandler,\
                                   kwargs['inf'], kwargs['channel_name'])
        self.server.allow_reuse_address = True
        super().__init__()

    def run(self):
        print("Starting sender server on port ", self.port)
        self.server.serve_forever()
        print("Now im shutting down!")

class SenderHandler(socketserver.BaseRequestHandler):
    def handle(self):
        #gen = generator_in_bytes(self.server.channel_name, self.server.inf)
        print("Socket for connection: ", self.request)
        while self.request:
            data=b"".join(next(self.server.gen))
            try:
                self.request.send(data)
                #sleep(0.05)
            except BrokenPipeError:
                print("Remote end shut off")
                break

class SenderServer(socketserver.ForkingTCPServer):
    def __init__(self, address, handler, inf, channel_name):
        super().__init__(address, handler)
        self.gen = generator_in_bytes(8, 'INF', 1024)

    def server_bind(self):
        self.socket.setsockopt(socketserver.socket.SOL_SOCKET,\
                               socketserver.socket.SO_REUSEADDR, 1)
        super().server_bind()
