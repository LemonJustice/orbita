# some helpers
def is_callable(obj):
    return hasattr(obj, '__call__')

def is_iterable(obj):
    return hasattr(obj, '__iter__')
def flat(list_arg):
    for l in list_arg:
        if isinstance(l, list):
            for l1 in flat(l):
                yield l1
        else:
            yield l

