host, port = 'localhost', 36000

import socket

if __name__ == '__main__':
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
    print("Connected")
    i = 0
    while True:
        try:
            s.recv(256)
            i+=1
            print("received", i)
        except:
            s.close()
