from orbita_bitstring import Bitstring as bitstring
import orbita_signals
import functools

# Constants:
cycles = 4
groups = 32
phrases = 128

def seq_multiply(iterable):
    '''Последовательное умножение элементов iterable.
    seq_multiply(range(1,5)) -> 60 '''
    return functools.reduce(lambda x, y: x*y, iterable)


class Sublist:
    def __init__(self, orig_list, start, end):
        self.orig_list = orig_list
        self.start, self.end = start, end

    def __len__(self):
        return (self.end - self.start)

    def __setitem__(self, key, val):
        if key < 0:
            self.orig_list[self.end+key] = val
        else:
            self.orig_list[self.start+key] = val

    def __getitem__(self, key):
        if isinstance(key, slice):
            start = key.start if key.start else 0
            end = key.stop if key.stop else len(self)
            return self.orig_list[self.start+start:self.start+end]
        if key < 0:
            return self.orig_list[self.end+key]
        else:
            return self.orig_list[self.start+key]

    def __iter__(self):
        return iter(self.orig_list[self.start:self.end])

    def __repr__(self):
        return 'Sublist:'+','.join('{!r}'.format(e) for e in self)

class Cadre:
    units_list = [groups, phrases, cycles]

    def __init__(self, inf, channel_type):
        self.channel_type = channel_type
        self.__inf = inf
        # порядок в этом списке имеет значение - от меньшего юнита к большему
        self.value = list(self.empty_word() \
                          for i in range(cycles*groups*phrases*inf))

    def empty_word(self):
        word = bitstring(int(0b0000000000000000),16)
        return word
        # 2х байтные пустые слова, старшим байтом вперед
        # здесь первый байт 0, 1 - бит дост.гр.

    def unit_iterator(self,level):
        # Это не слишком очевидно: level - уровень юнита в структуре, начиная с наименьшего.
        # Так, уровень фраз - 0, групп - 1, циклов - 2
        # Соответствие можно увидеть в unit_list
        total = seq_multiply(self.units_list[level::])
        step = len(self.value)//total
        result = list(Sublist(self.value, counter, counter+step) \
                      for counter in range(0,len(self.value), step))
        return result

    def fill_cycle_marks(self):
        cycles = self.unit_iterator(2)
        # в bitstring нумерация бит от младшего к старшему (уже нет)
        # бит для маркера цикла - 14
        for c in cycles:
            c[0].set_bit(1, True) # выставляем в 1 слове каждого цикла

    def fill_group_marks(self):
        groups = self.unit_iterator(1)
        # все круто, синхронизация
        for g in groups:
            g[0].set_bit(0, True)
            g[0].set_bit(8, True)

    def fill_analog(self, signal):
        cadre = self.value
        fw = signal.fw
        step = signal.step
        bits = signal.bits
        signal_values_generator = signal.fetch_bit()
        word_indices = range(fw, len(cadre),step)

        for i in word_indices:
            for b in bits:
                val = next(signal_values_generator)
                cadre[i].set_bit(b, val)

    def fill_digital(self, signal):
        cadre = self.value
        word_indices = signal.address_space
        bits = signal.bits
        signal_values_generator = signal.fetch_bit()

        for i in word_indices:
            for b in bits:
                val = next(signal_values_generator)
                cadre[i].set_bit(b, val)

    def fill_VT(self, signal):
        bits = signal.bits
        phrases_indices = signal.phrases
        phrases_list = self.unit_iterator(0)
        signal_values_generator = signal.bit_generator

        for i in phrases_indices:
            for b in bits:
                phrases_list[i][0].set_bit(b, next(signal_values_generator))

def cadre_creator(inf, channel_name):
    import available_signals
    c = Cadre(inf, channel_name)
    #c.fill_group_marks()
    c.fill_cycle_marks()
    for a in available_signals.analog:
        c.fill_analog(a)
    for d in available_signals.digital:
        c.fill_digital(d)

    while True:
        yield c.value 

def generator_in_bytes(inf, channel_name,size):
    words_count = size // 2 # в одном слове 2 байта
    current_buffer = []
    cadre_gen = cadre_creator(inf, channel_name)
    while True:
        if len(current_buffer) < words_count:
            current_buffer.extend(next(cadre_gen))
        return_bytes = list(current_buffer[i].val.to_bytes(2, byteorder='big') for i in range(0, words_count))
        del(current_buffer[0:words_count])
        yield return_bytes
