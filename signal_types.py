from enum import Enum

class ANP(Enum):
   T01 = [3, 4, 5, 6, 7, 10, 11, 12, 13]

class TMP(Enum):
   T11 = [3, 4, 5, 6, 7, 10, 11, 12, 13, 14]

class SGP(Enum):
   T01 = [3]
   T02 = [4]
   T03 = [5]
   T04 = [6]
   T06 = [7]
   T08 = [10]
   T09 = [11]
   T10 = [12]
   T11 = [13]

class CFP(Enum):
   T01 = [3]
   T02 = [4]
   T03 = [5]
   T04 = [6]
   T06 = [7]
   T08 = [10]
   T09 = [11]
   T10 = [12]
   T11 = [13]

class BMP21(Enum):
    T01 = [3, 4, 5, 6, 7, 10, 11, 12]

class BMP22(Enum):
    T01 = [3, 4, 5, 6, 7, 10]
    T02 = [11, 12, 14, 13, 15, 2]

class BMP23(Enum):
    T01 = [3, 4, 5, 6]
    T02 = [7, 10, 11, 13]
    T03 = [12, 14, 15, 2]
