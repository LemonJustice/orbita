import asyncio
import socket

rsock, wsock = socket.socketpair()
global i
i=0
loop = asyncio.get_event_loop()

class SenderProto(asyncio.Protocol):
    def connection_made(self, transport):
        print("im reading it from socket!")
        print(loop)
        loop.remove_reader(rsock)
        self.sock = transport.get_extra_info('socket')
        self.transport = transport
        def myReader():
            self.data = rsock.recv(256)
            self.transport.write(self.data)
        def myWriter():
            print("writing to client")
            self.sock.sendall(self.data)
        loop.add_reader(rsock, myReader)
        #loop.add_writer(self.sock, myWriter)

    def connection_lost(self, exc):
        loop.remove_writer(self.sock)
        loop.add_reader(rsock, globalReader)

def inReader(s):
    global i
    #print("i get data!", i)
    i+=1
    data = s.recv(256)
    wsock.sendall(data)

def globalReader():
    print("Im receiving!")
    rsock.recv(256)

if __name__ == '__main__':
    host, port = 'localhost', 40001
    inSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    inSock.connect((host, port))

    loop.add_reader(inSock, inReader, inSock)
    loop.add_reader(rsock, globalReader)

    port1 = 36000
    serverCoro = loop.create_server(SenderProto, host, port1)
    loop.run_until_complete(serverCoro)
    loop.run_forever()
    pass
